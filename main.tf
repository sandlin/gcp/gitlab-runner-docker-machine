# Set the runner name.
locals {
  runner_name = (var.runner_name != "" ? var.runner_name : "gcp-${var.gcp_project}" )
}


#
# This file defines the manager of the runners
#

# Create the Service Account to be used by the manager. This will spawn runner instances.
resource "google_service_account" "manager" {
  project      = var.gcp_project
  account_id   = "${var.resource_prefix}-manager"
  display_name = "GitLab Runner manager"
}

# Grant manager the necessary privs.
resource "google_project_iam_member" "manager_logwriter" {
  project = var.gcp_project
  role    = "roles/logging.logWriter"
  member  = "serviceAccount:${google_service_account.manager.email}"
}
resource "google_project_iam_member" "manager_instanceadmin" {
  project = var.gcp_project
  role    = "roles/compute.instanceAdmin.v1"
  member  = "serviceAccount:${google_service_account.manager.email}"
}
resource "google_project_iam_member" "manager_networkadmin" {
  project = var.gcp_project
  role    = "roles/compute.networkAdmin"
  member  = "serviceAccount:${google_service_account.manager.email}"
}
resource "google_project_iam_member" "manager_securityadmin" {
  project = var.gcp_project
  role    = "roles/compute.securityAdmin"
  member  = "serviceAccount:${google_service_account.manager.email}"
}

resource "google_project_iam_member" "manager_storage" {
  project = var.gcp_project
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.manager.email}"
}

#
# This file defines the account used by the actual runner nodes.
#

# Service account to be used by spawned runners
resource "google_service_account" "runner" {
  project      = var.gcp_project
  account_id   = "${var.resource_prefix}-runner"
  display_name = "GitLab Runner"
}

resource "google_project_iam_member" "runner_storage_admin" {
  project = var.gcp_project
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.runner.email}"
}
# Allow GitLab CI runner to use the runner service account.
resource "google_service_account_iam_member" "runner_sa_manager" {
  service_account_id = google_service_account.runner.name
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.manager.email}"
}


# Now let's handle the compute
resource "google_compute_instance" "manager" {
  project      = var.gcp_project
  name         = "${var.resource_prefix}-manager"
  machine_type = var.manager_instance_type
  zone         = var.gcp_zone

  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
      size  = var.manager_disk_size
      type  = "pd-standard"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = <<SCRIPT
set -e

echo "-----------------------------------------"
echo "Installing GitLab CI Runner"
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
sudo yum install -y gitlab-runner
echo "-----------------------------------------"

echo "-----------------------------------------"
echo "Installing docker machine."
curl -L https://github.com/docker/machine/releases/download/v0.16.2/docker-machine-Linux-x86_64 -o /tmp/docker-machine
sudo install /tmp/docker-machine /usr/local/bin/docker-machine
echo "-----------------------------------------"

echo "-----------------------------------------"
echo "Verifying docker-machine and generating SSH keys ahead of time."
docker-machine create --driver google \
    --google-project ${var.gcp_project} \
    --google-machine-type f1-micro \
    --google-zone ${var.gcp_zone} \
    --google-service-account ${google_service_account.runner.email} \
    --google-scopes https://www.googleapis.com/auth/cloud-platform \
    --google-disk-type pd-ssd \
    --google-disk-size ${var.runner_disk_size} \
    --google-tags ${var.runner_gcp_tags} \
    --google-use-internal-ip \
    ${var.resource_prefix}-test
echo "-----------------------------------------"

docker-machine rm -y ${var.resource_prefix}-test

echo "Setting GitLab concurrency"
sed -i "s/concurrent = .*/concurrent = ${var.max_concurrent_runners}/" /etc/gitlab-runner/config.toml

echo "Registering GitLab CI runner with GitLab instance."
sudo gitlab-runner register -n \
    --name "${local.runner_name}" \
    --url ${var.gitlab_url} \
    --registration-token ${var.runner_registration_token} \
    --executor "docker+machine" \
    --docker-image "alpine:latest" \
    --tag-list "${var.manager_tags}" \
    --run-untagged="${var.manager_allow_untagged}" \
    --docker-privileged=${var.docker_privileged} \
    --machine-idle-time ${var.runner_idle_time} \
    --machine-machine-driver google \
    --machine-machine-name "${var.resource_prefix}-runner-%s" \
    --machine-machine-options "google-project=${var.gcp_project}" \
    --machine-machine-options "google-machine-type=${var.runner_instance_type}" \
    --machine-machine-options "google-zone=${var.gcp_zone}" \
    --machine-machine-options "google-service-account=${google_service_account.runner.email}" \
    --machine-machine-options "google-scopes=https://www.googleapis.com/auth/cloud-platform" \
    --machine-machine-options "google-disk-type=pd-ssd" \
    --machine-machine-options "google-disk-size=${var.runner_disk_size}" \
    --machine-machine-options "google-tags=${var.runner_gcp_tags}" \
    --machine-machine-options "google-use-internal-ip" \
    && true

echo "GitLab CI Runner installation complete"
SCRIPT

  service_account {
    email  = google_service_account.manager.email
    scopes = ["cloud-platform"]
  }
}