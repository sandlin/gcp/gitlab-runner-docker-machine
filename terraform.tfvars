gitlab_url = "http://34.145.52.149"
gcp_project = "jsandlin-c9fe7132"
gcp_region = "us-west1"
gcp_zone = "us-west1-a"
manager_owner = "jsandlin"
resource_prefix = "glrunner"

service_account_file = "/Users/jsandlin/.gcp/glrunner-sa.json"
# TOKEN obtained from http://34.145.52.149/admin/workers
