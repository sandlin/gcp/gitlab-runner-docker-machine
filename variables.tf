variable "gcp_project" {
  description = "The GCP Project"
  default = ""
}

variable "gcp_region" {
    description = "Region this goes to"
    default = ""
}

variable "gcp_zone" {
    description = "Zone this goes to"
    default = ""
}

variable "gitlab_url" {
    description = "The actual URL of your instance"
    default = ""
}

variable "service_account_json" {
    description = "The value of the json creds file. Will be used by GitLab project vars to pass creds (until we have vault in place)"
    default = ""
}
variable "service_account_file" {
    description = "The service account id to use; this value will only be used if service_account_json is not set."
    default = ""
}
variable "runner_registration_token" {
    description = <<EOF
        The token, obtained via GitLab UI, to register runner to app
        This value will be pulled from the GitLab hidden vars (until I have vault in place)
    EOF
}

variable "resource_prefix" {
    description = "The prefix to apply to resource names"
    default = ""
}
variable "runner_name" {
    description = "The name of the runner"
    default = "gitlab-runner"
}

variable "manager_disk_size" {
    description = "The manager disk size in gigs"
    default = "20"
}

variable "manager_owner" {
    description = "The name in GitLab under which to register the manager."
    default = ""
}

variable "manager_tags" {
    description = "Tags to assign to the manager in GitLab"
    default = ""
}

variable "manager_allow_untagged" {
    description = "Should the manager also be allowed to run non-tagged jobs?"
    default = "true"
}

variable "manager_instance_type" {
    description = "GC Instance type for manager."
    default = "f1-micro"
}

variable "max_concurrent_runners" {
    type = number
    default = 10
    description = "The maximum number of concurrent runner nodes"
}

variable "runner_disk_size" {
    description = "The runner disk size in gigs"
    default = "20"
}

variable "runner_idle_time" {
    description = "Shut down runners after _ idle time"
    default = 300
}

variable "runner_gcp_tags" {
    description = "The GCP Networking tags to apply to the runners"
    default = "gitlab-runner"
}

variable "runner_instance_type" {
    description = "The GCP instance type of runners"
    default = "n1-standard-1"
}

variable "docker_privileged" {
    type = string
    default = "true"
    description = "Grant privs to containers to run Docker in Docker"
}