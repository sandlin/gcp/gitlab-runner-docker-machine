
output "manager_service_account" {
    value = {
        # id => an identifier for the resource with format projects/{{project}}/serviceAccounts/{{email}}
        id = google_service_account.manager.id
        # name => The fully-qualified name of the service account.
        name = google_service_account.manager.name
        # display_name => The display name for the service account. Can be updated without creating a new resource.
        display_name = google_service_account.manager.display_name
        # unique_id => The unique id of the service account.
        unique_id = google_service_account.manager.unique_id
    }
}


output "runner_service_account" {
    value = {
        # id => an identifier for the resource with format projects/{{project}}/serviceAccounts/{{email}}
        id = google_service_account.runner.id
        # name => The fully-qualified name of the service account.
        name = google_service_account.runner.name
        # display_name => The display name for the service account. Can be updated without creating a new resource.
        display_name = google_service_account.runner.display_name
        # unique_id => The unique id of the service account.
        unique_id = google_service_account.runner.unique_id
    }
}

output "manager_instance" {
    description = "The instance which is the manager"
    value = google_compute_instance.manager.name
}

output "ssh" {
    description = "SSH cmd to access the manager"
    value = "gcloud compute ssh ${var.manager_owner}@${google_compute_instance.manager.name}"
}